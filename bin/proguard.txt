# view AndroidManifest.xml #generated:167
# view AndroidManifest.xml #generated:31
-keep class android.test.InstrumentationTestRunner { <init>(...); }

# view AndroidManifest.xml #generated:128
-keep class cn.jpush.android.service.AlarmReceiver { <init>(...); }

# view AndroidManifest.xml #generated:85
-keep class cn.jpush.android.service.DownloadService { <init>(...); }

# view AndroidManifest.xml #generated:107
-keep class cn.jpush.android.service.PushReceiver { <init>(...); }

# view AndroidManifest.xml #generated:93
-keep class cn.jpush.android.service.PushService { <init>(...); }

# view AndroidManifest.xml #generated:74
-keep class cn.jpush.android.ui.PushActivity { <init>(...); }

# view AndroidManifest.xml #generated:44
-keep class com.example.jpushdemo.ExampleApplication { <init>(...); }

# view AndroidManifest.xml #generated:51
-keep class com.example.jpushdemo.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:131
-keep class com.example.jpushdemo.MyReceiver { <init>(...); }

# view AndroidManifest.xml #generated:61
-keep class com.example.jpushdemo.PushSetActivity { <init>(...); }

# view AndroidManifest.xml #generated:154
-keep class com.example.jpushdemo.ResStartService { <init>(...); }

# view AndroidManifest.xml #generated:63
-keep class com.example.jpushdemo.SettingActivity { <init>(...); }

# view AndroidManifest.xml #generated:65
-keep class com.example.jpushdemo.TestActivity { <init>(...); }

